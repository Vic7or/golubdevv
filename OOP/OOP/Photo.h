#pragma once
#include <windows.h>
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// ���������� ���������
delegate void OneDelegat();
delegate void TwoDelegat();

// �������� ������
ref class Photo
{
// ����
private:
	int member; // ����� ������
	int format; // ������
	double razresh; // ����������
	unsigned int kolSnim; // ���������� �������
	int batt; // ����� �������
	unsigned short timeWork; // ����� ������

	// ��������
public:
	property int p_member {
		int get() {
			return member;
		}
		void set(int a) {
			member = a;
		}
	}
	property int p_format {
		int get() {
			return format;
		}
		void set (int b) {
			format = b;
		}
	}
	property double p_razresh {
		double get() {
			double mp;
		if (format == 0) {
			if (razresh == 0)
				mp = 4.2;
			if (razresh == 1)
				mp = 4.8;
			if (razresh == 2)
				mp = 5.4;
			if (razresh == 3)
				mp = 6;
			if (razresh == 4)
				mp = 7.2;
		}
		else {
			if (razresh == 0)
				mp = 41.6;
			if (razresh == 1)
				mp = 47.6;
			if (razresh == 2)
				mp = 52.6;
			if (razresh == 3)
				mp = 58.8;
			if (razresh == 4)
				mp = 71.4;
		}
			return mp;
		}
		void set(double c) {
			razresh = c;
		}
	}
	property unsigned int p_kolSnim {
		unsigned int get() {
			return kolSnim;
		}
		void set(unsigned int d) {
			kolSnim = d;
		}
	}
	property int p_batt {
		int get() {
			return batt;
		}
		void set(int e) {
			batt = e;
		}	
	}
	property unsigned short p_timeWork {
		unsigned short get() {
			if (batt == 0) {
				timeWork = 114;
			}
			if (batt == 1) {
				timeWork = 172;
			}
			if (batt == 2) {
				timeWork = 248;
			}
			if (batt == 3) {
				timeWork = 300;
			}
			return timeWork;
		}
		void set(unsigned short f) {
				timeWork = f;
			}
	}

	// ������
public:
	void del_member(TextBox^ txtKol, double razr) {
		kolSnim = member/razr;
		txtKol->Text = Convert::ToString(kolSnim);
	}
	void siemka(int txtTimeWork, TextBox^ textboxKol, TextBox^ textboxRab, ProgressBar^ bar, Timer^ tmr1, PictureBox^ picPh) {
		if (kolSnim == 0) {
			tmr1->Enabled = false;
			picPh->Visible = false;
			MessM();
		}
		if (timeWork == 0) {
			tmr1->Enabled = false;
			picPh->Visible = false;
			MessCh();
		}
		if (kolSnim != 0 && timeWork != 0) {
			kolSnim--;
			timeWork = txtTimeWork - 2;
			bar->Value = timeWork;
			textboxKol->Text = Convert::ToString(kolSnim);
			textboxRab->Text = Convert::ToString(timeWork);
		}
	}
	void charge(int txttimeWork, TextBox^ txtRab, ProgressBar^ bar, Timer^ tmr2) {
		if (p_timeWork == txttimeWork) {
			tmr2->Enabled = false;
			MessageBox::Show("����������� �������!", "", MessageBoxButtons::OK, 
								MessageBoxIcon::None);
		}
		else {
			timeWork = txttimeWork + 2;	
			bar->Value = timeWork;
			txtRab->Text = Convert::ToString(timeWork);
		}
	}

	// ������ ��� �������
	static void messMemb() {
		MessageBox::Show("������ ���������!", "", MessageBoxButtons::OK, 
									MessageBoxIcon::Warning);
	}
	static void messChar() {
		MessageBox::Show("���������� ���������!", "", MessageBoxButtons::OK, 
									MessageBoxIcon::Warning);
	}

	// �������
	event OneDelegat^ MessM;
	event TwoDelegat^ MessCh;

public:
	Photo(void);
};
	





