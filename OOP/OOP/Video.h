#pragma once
#include "photo.h"

ref class Video :
public Photo
{
// ����
private:
	double razresh2; // ��������������� ���� ����������
	unsigned short timeWork2; // ��������������� ���� ����� ������

// ��������
public:
	property double p_razresh2 { // ��������������� �������� ����������
		double get() {
			double mp;
			if (p_format == 0) {
				if (razresh2 == 0)
					mp = 15;
				if (razresh2 == 1)
					mp = 45;
				if (razresh2 == 2)
					mp = 60;
				if (razresh2 == 3)
					mp = 102;
			}
			else {
				if (razresh2 == 0)
					mp = 30;
				if (razresh2 == 1)
					mp = 90;
				if (razresh2 == 2)
					mp = 120;
				if (razresh2 == 3)
					mp = 204;
			}
			return mp;
		}
		void set(double c) {
			razresh2 = c;
		}
	}
	property unsigned short p_timeWork2 { // ��������������� �������� ����� ������
		unsigned short get() {
			if (p_batt == 0) {
				timeWork2 = 88;
			}
			if (p_batt == 1) {
				timeWork2 = 112;
			}
			if (p_batt == 2) {
				timeWork2 = 160;
			}
			if (p_batt == 3) {
				timeWork2 = 180;
			}
			return timeWork2;
		}
		void set(unsigned short f) {
				timeWork2 = f;
			}
	}
	// ������
public:
	void siemka(int txtTimeWork, TextBox^ textboxKol, TextBox^ textboxRab, ProgressBar^ bar, Timer^ tmr3, PictureBox^ picRec, PictureBox^ picVkl2, Button^ btnVideo) { // ��������������� ������ ������
		if (p_kolSnim == 0) {
			tmr3->Enabled = false;
			picRec->Visible = false;
			picVkl2->Visible = true;
			btnVideo->Text = L"������";
			MessM();
		}
		if (timeWork2 == 0) {
			tmr3->Enabled = false;
			picRec->Visible = false;
			picVkl2->Visible = true;
			btnVideo->Text = L"������";
			MessCh();
		}
		if (p_kolSnim != 0 && txtTimeWork != 0) {
			p_kolSnim--;
			timeWork2 = txtTimeWork - 2;
			bar->Value = timeWork2;
			textboxKol->Text = Convert::ToString(p_kolSnim);
			textboxRab->Text = Convert::ToString(timeWork2);
		}
		
	}
	void charge(int txtTimeW, TextBox^ txtRab, ProgressBar^ bar, Timer^ tmr4) { // ��������������� ������ �������
		if (p_timeWork2 == txtTimeW) {
			tmr4->Enabled = false;
			MessageBox::Show("����������� ��������!", "", MessageBoxButtons::OK, 
								MessageBoxIcon::None);
		}
		else {
			timeWork2 = txtTimeW + 2;	
			bar->Value = timeWork2;
			txtRab->Text = Convert::ToString(timeWork2);
		}
	}
public:
	Video(void);
};

