#pragma once
#include <cmath>
#include <string>
#include <sstream>

using namespace std;

namespace Konvert {


	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Globalization;
	using namespace System::Net;
	using namespace System::IO;

	int x1;     // ��������������� ���������� (������� �� ����� � ������)
	double x3;  // �������� �������������� �����
	char ds;    // ������ ����������� �����������
	double k1,k2,k3,k4;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected: 
	private: System::Windows::Forms::TextBox^  txtKurs;
	private: System::Windows::Forms::TextBox^  txtSum;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::RadioButton^  rbRD;

	private: System::Windows::Forms::RadioButton^  rbDR;

	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::TextBox^  txt2;

	private: System::Windows::Forms::TextBox^  txt1;
	private: System::Windows::Forms::Label^  lbl2;

	private: System::Windows::Forms::Label^  lbl1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  btnKonv;
	private: System::Windows::Forms::Button^  btnClose;


	private: System::Windows::Forms::RadioButton^  rbER;
	private: System::Windows::Forms::RadioButton^  rbRE;





	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->txtKurs = (gcnew System::Windows::Forms::TextBox());
			this->txtSum = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->rbRE = (gcnew System::Windows::Forms::RadioButton());
			this->rbER = (gcnew System::Windows::Forms::RadioButton());
			this->rbRD = (gcnew System::Windows::Forms::RadioButton());
			this->rbDR = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->lbl2 = (gcnew System::Windows::Forms::Label());
			this->lbl1 = (gcnew System::Windows::Forms::Label());
			this->txt2 = (gcnew System::Windows::Forms::TextBox());
			this->txt1 = (gcnew System::Windows::Forms::TextBox());
			this->btnKonv = (gcnew System::Windows::Forms::Button());
			this->btnClose = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->txtKurs);
			this->groupBox1->Controls->Add(this->txtSum);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->groupBox1->ForeColor = System::Drawing::SystemColors::HotTrack;
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(271, 119);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"�������� ������";
			// 
			// txtKurs
			// 
			this->txtKurs->Location = System::Drawing::Point(184, 70);
			this->txtKurs->Name = L"txtKurs";
			this->txtKurs->Size = System::Drawing::Size(81, 22);
			this->txtKurs->TabIndex = 2;
			this->txtKurs->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->txtKurs->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtKurs_KeyPress);
			this->txtKurs->Leave += gcnew System::EventHandler(this, &Form1::txtKurs_Leave);
			// 
			// txtSum
			// 
			this->txtSum->Location = System::Drawing::Point(181, 27);
			this->txtSum->Name = L"txtSum";
			this->txtSum->Size = System::Drawing::Size(81, 22);
			this->txtSum->TabIndex = 3;
			this->txtSum->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->txtSum->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtSum_KeyPress);
			this->txtSum->Leave += gcnew System::EventHandler(this, &Form1::txtSum_Leave);
			// 
			// label2
			// 
			this->label2->BackColor = System::Drawing::SystemColors::Control;
			this->label2->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label2->ForeColor = System::Drawing::SystemColors::Desktop;
			this->label2->Location = System::Drawing::Point(9, 67);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(150, 27);
			this->label2->TabIndex = 1;
			this->label2->Text = L"���� ������ (���./$):";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::SystemColors::Desktop;
			this->label1->Location = System::Drawing::Point(9, 24);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(147, 27);
			this->label1->TabIndex = 0;
			this->label1->Text = L"����� ��� ����������� :";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->rbRE);
			this->groupBox2->Controls->Add(this->rbER);
			this->groupBox2->Controls->Add(this->rbRD);
			this->groupBox2->Controls->Add(this->rbDR);
			this->groupBox2->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->groupBox2->ForeColor = System::Drawing::SystemColors::HotTrack;
			this->groupBox2->Location = System::Drawing::Point(289, 12);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(200, 119);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"����������� �����������";
			// 
			// rbRE
			// 
			this->rbRE->AutoSize = true;
			this->rbRE->ForeColor = System::Drawing::SystemColors::Desktop;
			this->rbRE->Location = System::Drawing::Point(6, 94);
			this->rbRE->Name = L"rbRE";
			this->rbRE->Size = System::Drawing::Size(97, 19);
			this->rbRE->TabIndex = 3;
			this->rbRE->TabStop = true;
			this->rbRE->Text = L"����� � ����";
			this->rbRE->UseVisualStyleBackColor = true;
			this->rbRE->CheckedChanged += gcnew System::EventHandler(this, &Form1::rbRE_CheckedChanged);
			// 
			// rbER
			// 
			this->rbER->AutoSize = true;
			this->rbER->ForeColor = System::Drawing::SystemColors::Desktop;
			this->rbER->Location = System::Drawing::Point(6, 70);
			this->rbER->Name = L"rbER";
			this->rbER->Size = System::Drawing::Size(98, 19);
			this->rbER->TabIndex = 2;
			this->rbER->TabStop = true;
			this->rbER->Text = L"���� � �����";
			this->rbER->UseVisualStyleBackColor = true;
			this->rbER->CheckedChanged += gcnew System::EventHandler(this, &Form1::rbER_CheckedChanged);
			// 
			// rbRD
			// 
			this->rbRD->AutoSize = true;
			this->rbRD->ForeColor = System::Drawing::SystemColors::Desktop;
			this->rbRD->Location = System::Drawing::Point(6, 45);
			this->rbRD->Name = L"rbRD";
			this->rbRD->Size = System::Drawing::Size(118, 19);
			this->rbRD->TabIndex = 1;
			this->rbRD->TabStop = true;
			this->rbRD->Text = L"����� � �������";
			this->rbRD->UseVisualStyleBackColor = true;
			this->rbRD->CheckedChanged += gcnew System::EventHandler(this, &Form1::rbRD_CheckedChanged);
			// 
			// rbDR
			// 
			this->rbDR->AutoSize = true;
			this->rbDR->ForeColor = System::Drawing::SystemColors::Desktop;
			this->rbDR->Location = System::Drawing::Point(6, 22);
			this->rbDR->Name = L"rbDR";
			this->rbDR->Size = System::Drawing::Size(121, 19);
			this->rbDR->TabIndex = 0;
			this->rbDR->TabStop = true;
			this->rbDR->Text = L"������� � �����";
			this->rbDR->UseVisualStyleBackColor = true;
			this->rbDR->CheckedChanged += gcnew System::EventHandler(this, &Form1::rbDR_CheckedChanged);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label5);
			this->groupBox3->Controls->Add(this->lbl2);
			this->groupBox3->Controls->Add(this->lbl1);
			this->groupBox3->Controls->Add(this->txt2);
			this->groupBox3->Controls->Add(this->txt1);
			this->groupBox3->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->groupBox3->ForeColor = System::Drawing::SystemColors::HotTrack;
			this->groupBox3->Location = System::Drawing::Point(13, 126);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(270, 59);
			this->groupBox3->TabIndex = 2;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"��������� �����������";
			// 
			// label5
			// 
			this->label5->ForeColor = System::Drawing::SystemColors::Desktop;
			this->label5->Location = System::Drawing::Point(117, 23);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 23);
			this->label5->TabIndex = 4;
			this->label5->Text = L"=";
			this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// lbl2
			// 
			this->lbl2->ForeColor = System::Drawing::SystemColors::Desktop;
			this->lbl2->Location = System::Drawing::Point(229, 22);
			this->lbl2->Name = L"lbl2";
			this->lbl2->Size = System::Drawing::Size(35, 23);
			this->lbl2->TabIndex = 3;
			this->lbl2->Text = L"���.";
			this->lbl2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// lbl1
			// 
			this->lbl1->ForeColor = System::Drawing::SystemColors::Desktop;
			this->lbl1->Location = System::Drawing::Point(82, 23);
			this->lbl1->Name = L"lbl1";
			this->lbl1->Size = System::Drawing::Size(35, 23);
			this->lbl1->TabIndex = 2;
			this->lbl1->Text = L"$";
			this->lbl1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txt2
			// 
			this->txt2->Location = System::Drawing::Point(155, 22);
			this->txt2->Name = L"txt2";
			this->txt2->ReadOnly = true;
			this->txt2->Size = System::Drawing::Size(68, 22);
			this->txt2->TabIndex = 1;
			this->txt2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// txt1
			// 
			this->txt1->Location = System::Drawing::Point(8, 23);
			this->txt1->Name = L"txt1";
			this->txt1->ReadOnly = true;
			this->txt1->Size = System::Drawing::Size(68, 22);
			this->txt1->TabIndex = 0;
			this->txt1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// btnKonv
			// 
			this->btnKonv->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->btnKonv->Location = System::Drawing::Point(289, 153);
			this->btnKonv->Name = L"btnKonv";
			this->btnKonv->Size = System::Drawing::Size(118, 32);
			this->btnKonv->TabIndex = 3;
			this->btnKonv->Text = L"��������������";
			this->btnKonv->UseVisualStyleBackColor = true;
			this->btnKonv->Click += gcnew System::EventHandler(this, &Form1::btnKonv_Click);
			// 
			// btnClose
			// 
			this->btnClose->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->btnClose->Location = System::Drawing::Point(413, 154);
			this->btnClose->Name = L"btnClose";
			this->btnClose->Size = System::Drawing::Size(76, 32);
			this->btnClose->TabIndex = 4;
			this->btnClose->Text = L"�������";
			this->btnClose->UseVisualStyleBackColor = true;
			this->btnClose->Click += gcnew System::EventHandler(this, &Form1::btnClose_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(497, 197);
			this->Controls->Add(this->btnClose);
			this->Controls->Add(this->btnKonv);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"Form1";
			this->Text = L"APM ��������� ��������� ������";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion





	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 rbDR->Checked=true;
				 // ����������� ����������� ����������� (����� ��� �������)
				 // �� ������������ ��������.
				 NumberFormatInfo^ nfi = NumberFormatInfo::CurrentInfo;
				 ds = (char)nfi->NumberDecimalSeparator[0];
			 }
	private: System::Void rbDR_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 this->txtKurs->Text = Convert::ToString(k1);
				 int l=txtKurs->Text->Length;
				 int d=txtKurs->Text->IndexOf(ds);
				 if (txtKurs->Text->IndexOf(ds) != -1)
				 {				
					 if (l-2==d)
					 {
						 txtKurs->Text = txtKurs->Text->Insert(l,"0");
					 }				 
				 }
				 if (txtKurs->Text->IndexOf(ds) == -1)
				 {
					 txtKurs->Text = txtKurs->Text->Insert(l,",00");
				 }
				 this->txtSum->Text = L"";
				 this->label2->Text=L"���� ������ (���./$):";
				 this->lbl1->Text = L"$";
				 this->lbl2->Text = L"���.";
				 this->txt1->Text = L"";
				 this->txt2->Text = L"";
			 }

	private: System::Void rbRD_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 this->txtKurs->Text = Convert::ToString(k2);
				 int l=txtKurs->Text->Length;
				 int d=txtKurs->Text->IndexOf(ds);
				 if (txtKurs->Text->IndexOf(ds) != -1)
				 {				
					 if (l-2==d)
					 {
						 txtKurs->Text = txtKurs->Text->Insert(l,"0");
					 }				 
				 }
				 if (txtKurs->Text->IndexOf(ds) == -1)
				 {
					 txtKurs->Text = txtKurs->Text->Insert(l,",00");
				 }
				 this->txtSum->Text = L"";
				 this->label2->Text=L"���� ������ (���./$):";
				 this->lbl1->Text = L"���.";
				 this->lbl2->Text = L"$";
				 this->txt1->Text = L"";
				 this->txt2->Text = L"";
			 }
	private: System::Void rbER_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 this->txtKurs->Text = Convert::ToString(k3);
				 int l=txtKurs->Text->Length;
				 int d=txtKurs->Text->IndexOf(ds);
				 if (txtKurs->Text->IndexOf(ds) != -1)
				 {				
					 if (l-2==d)
					 {
						 txtKurs->Text = txtKurs->Text->Insert(l,"0");
					 }				 
				 }
				 if (txtKurs->Text->IndexOf(ds) == -1)
				 {
					 txtKurs->Text = txtKurs->Text->Insert(l,",00");
				 }
				 this->txtSum->Text = L"";
				 this->label2->Text=L"���� ������ (���./�):";
				 this->lbl1->Text = L"�";
				 this->lbl2->Text = L"���.";
				 this->txt1->Text = L"";
				 this->txt2->Text = L"";
			 }

	private: System::Void rbRE_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 this->txtKurs->Text = Convert::ToString(k4);
				 int l=txtKurs->Text->Length;
				 int d=txtKurs->Text->IndexOf(ds);
				 if (txtKurs->Text->IndexOf(ds) != -1)
				 {				
					 if (l-2==d)
					 {
						 txtKurs->Text = txtKurs->Text->Insert(l,"0");
					 }				 
				 }
				 if (txtKurs->Text->IndexOf(ds) == -1)
				 {
					 txtKurs->Text = txtKurs->Text->Insert(l,",00");
				 }
				 this->txtSum->Text = L"";
				 this->label2->Text=L"���� ������ (���./�):";
				 this->lbl1->Text = L"���.";
				 this->lbl2->Text = L"�";
				 this->txt1->Text = L"";
				 this->txt2->Text = L"";
			 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
			 }
	private: System::Void btnKonv_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (txtSum->Text=="" || txtKurs->Text=="" || txtKurs->Text=="0" || txtSum->Text=="0,00")
				 {
					 if (txtSum->Text=="" || txtSum->Text=="0,00")
						 MessageBox::Show(this,"������� ����� ��� �����������.","��������������.",MessageBoxButtons::OK,MessageBoxIcon::Warning,MessageBoxDefaultButton::Button1);
					 if (txtKurs->Text=="" || txtKurs->Text=="0")
						 MessageBox::Show(this,"������� ���� ������.","��������������.",MessageBoxButtons::OK,MessageBoxIcon::Warning,MessageBoxDefaultButton::Button1);
				 }
				 else
				 {
					 double sum,rez,kurs;
					 sum = Convert::ToDouble(txtSum->Text);
					 kurs = Convert::ToDouble(txtKurs->Text);
					 if (rbDR->Checked==true)
					 {
						 rez=sum*kurs;
					 }
					 if (rbRD->Checked==true)
					 {
						 rez=sum/kurs;
					 }
					 if (rbER->Checked==true)
					 {
						 rez=sum*kurs;
					 }
					 if (rbRE->Checked==true)
					 {
						 rez=sum/kurs;
					 }
					 rez=floor(rez*100)/100;
					 String^ g1=String::Format("{0:F2}",sum);
					 String^ g2=String::Format("{0:F2}",rez);
					 txt1->Text = Convert::ToString(g1);
					 txt2->Text = Convert::ToString(g2);

				 }
			 }
	private: System::Void txtKurs_Leave(System::Object^  sender, System::EventArgs^  e) {
				 if (rbDR->Checked==true )
				 {
					 k1=Convert::ToDouble(txtKurs->Text);
				 }
				 if (rbRD->Checked==true)
				 {
					 k2=Convert::ToDouble(txtKurs->Text);
				 }
				 if (rbER->Checked==true)
				 {
					 k3=Convert::ToDouble(txtKurs->Text);
				 }
				 if (rbRE->Checked==true)
				 {
					 k4=Convert::ToDouble(txtKurs->Text);
				 }
				 int l=txtKurs->Text->Length;
				 int d=txtKurs->Text->IndexOf(ds);
				 if (txtKurs->Text->IndexOf(ds) != -1)
				 {				
					 if (l-2==d)
					 {
						 txtKurs->Text = txtKurs->Text->Insert(l,"0");
					 }				 
				 }
				 if (txtKurs->Text->IndexOf(ds) == -1)
				 {
					 txtKurs->Text = txtKurs->Text->Insert(l,",00");
				 }
			 }
	private: System::Void txtSum_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 // ���� ������� ������� �� ��������.
				 if (!Char::IsDigit(e->KeyChar))
					 // ������ �� ���� ����� ������ ����������� �����������.
					 if (e->KeyChar != ds || txtSum->Text->IndexOf(ds) != -1)
						 // ���� ������� ������� �� �������� �������� BackSpace.
						 if (e->KeyChar != (char)Keys::Back)
							 e->Handled = true;          // ������ �����

				 //������ �� ���� ���� ����� ������
				 if (e->KeyChar == (char)Keys::D0 || e->KeyChar == (char)Keys::NumPad0)  // ���� ������ ������� 0
					 if (txtSum->Text->Length >= 1)        // ���� ������ �� ������
						 if (txtSum->Text[0] == '0' && txtSum->SelectionStart < 2)
							 e->Handled = true;             // ������ �����

				 // ���� ����� ���� ���� ����� ������� ����
				 if (Char::IsDigit(e->KeyChar) && (e->KeyChar!= (char)Keys::D0 || e->KeyChar != (char)Keys::NumPad0))
					 if (txtSum->Text=="0")
					 {
						 txtSum->Text=L"";
						 txtSum->SelectionStart=0;
					 }
					 // ������ ����������� ����������� � ������ ����� �� "0,".
					 if (e->KeyChar == ds) {    
						 // ���� ������ ���������� �����������
						 x1 = 0;
						 if (txtSum->Text != "" && txtSum->Text[0] == '-') { x1 = 1; }
						 if (txtSum->Text->IndexOf(ds) == -1 && txtSum->SelectionStart == x1) {
							 // ���� ���������� ����������� ��� ����, �� 0 ��������� �� �����
							 txtSum->Text = txtSum->Text->Insert(x1, "0");  // ������� 0
							 txtSum->SelectionStart = x1 + 1;  // ��������� ������ � �����
						 }
					 }

			 }
	private: System::Void txtKurs_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 // ���� ������� ������� �� ��������.
				 if (!Char::IsDigit(e->KeyChar))
					 // ������ �� ���� ����� ������ ����������� �����������.
					 if (e->KeyChar != ds || txtKurs->Text->IndexOf(ds) != -1)
						 // ���� ������� ������� �� �������� �������� BackSpace.
						 if (e->KeyChar != (char)Keys::Back)
							 e->Handled = true;          // ������ �����

				 //������ �� ���� ���� ����� ������
				 if (e->KeyChar == (char)Keys::D0 || e->KeyChar == (char)Keys::NumPad0)  // ���� ������ ������� 0
					 if (txtKurs->Text->Length >= 1)        // ���� ������ �� ������
						 if (txtKurs->Text[0] == '0' && txtKurs->SelectionStart < 2)
							 e->Handled = true;             // ������ �����


				 // ���� ����� ���� ���� ����� ������� ����
				 if (Char::IsDigit(e->KeyChar) && (e->KeyChar!= (char)Keys::D0 || e->KeyChar != (char)Keys::NumPad0))
					 if (txtKurs->Text=="0")
					 {
						 txtKurs->Text=L"";
						 txtKurs->SelectionStart=0;
					 }
					 // ������ ����������� ����������� � ������ ����� �� "0,".
					 if (e->KeyChar == ds) {    
						 // ���� ������ ���������� �����������
						 x1 = 0;
						 if (txtKurs->Text != "" && txtKurs->Text[0] == '-') { x1 = 1; }
						 if (txtKurs->Text->IndexOf(ds) == -1 && txtKurs->SelectionStart == x1) {
							 // ���� ���������� ����������� ��� ����, �� 0 ��������� �� �����
							 txtKurs->Text = txtKurs->Text->Insert(x1, "0");  // ������� 0
							 txtKurs->SelectionStart = x1 + 1;  // ��������� ������ � �����
						 }
					 }


			 }
	private: System::Void txtSum_Leave(System::Object^  sender, System::EventArgs^  e) {
				 String^ g=String::Format("{0:F2}",Convert::ToDouble(txtSum->Text));
				 txtSum->Text = Convert::ToString(g);
			 }

	private: System::Void btnClose_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
			 }
	};

}

