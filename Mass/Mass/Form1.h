#pragma once
#include <sstream>
#include <cmath>
namespace Mass {
	
	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	int proverka;
	int trb;
	int deystvie[9]={0};
	int x1;     // ��������������� ���������� (������� �� ����� � ������)
	double x3;  // �������� �������������� �����

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
		
		
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected: 
	private: System::Windows::Forms::TextBox^  txtmin;
	private: System::Windows::Forms::TextBox^  txtmax;
	private: System::Windows::Forms::TextBox^  txtkol;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  lbl1;
	private: System::Windows::Forms::TextBox^  txtMass;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::RadioButton^  rb8;
	private: System::Windows::Forms::RadioButton^  rb7;
	private: System::Windows::Forms::RadioButton^  rb6;
	private: System::Windows::Forms::RadioButton^  rb5;
	private: System::Windows::Forms::RadioButton^  rb4;
	private: System::Windows::Forms::RadioButton^  rb3;
	private: System::Windows::Forms::RadioButton^  rb2;
	private: System::Windows::Forms::RadioButton^  rb1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  txtRes;
	private: System::Windows::Forms::Button^  btnClose;
	private: System::Windows::Forms::Button^  btnEnter;
	private: System::Windows::Forms::Button^  btngen;
	private: System::Windows::Forms::Button^  btnReset;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  txtFileIn;
	private: System::Windows::Forms::Button^  btnIn;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::TextBox^  txtFileOut;
	private: System::Windows::Forms::Button^  btnOut;




	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->txtmin = (gcnew System::Windows::Forms::TextBox());
			this->txtmax = (gcnew System::Windows::Forms::TextBox());
			this->txtkol = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->lbl1 = (gcnew System::Windows::Forms::Label());
			this->txtMass = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->rb8 = (gcnew System::Windows::Forms::RadioButton());
			this->rb7 = (gcnew System::Windows::Forms::RadioButton());
			this->rb6 = (gcnew System::Windows::Forms::RadioButton());
			this->rb5 = (gcnew System::Windows::Forms::RadioButton());
			this->rb4 = (gcnew System::Windows::Forms::RadioButton());
			this->rb3 = (gcnew System::Windows::Forms::RadioButton());
			this->rb2 = (gcnew System::Windows::Forms::RadioButton());
			this->rb1 = (gcnew System::Windows::Forms::RadioButton());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->txtRes = (gcnew System::Windows::Forms::TextBox());
			this->btnClose = (gcnew System::Windows::Forms::Button());
			this->btnEnter = (gcnew System::Windows::Forms::Button());
			this->btngen = (gcnew System::Windows::Forms::Button());
			this->btnReset = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->txtFileIn = (gcnew System::Windows::Forms::TextBox());
			this->btnIn = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->txtFileOut = (gcnew System::Windows::Forms::TextBox());
			this->btnOut = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->txtmin);
			this->groupBox1->Controls->Add(this->txtmax);
			this->groupBox1->Controls->Add(this->txtkol);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->lbl1);
			this->groupBox1->ForeColor = System::Drawing::SystemColors::HotTrack;
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(387, 132);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"�������� ������";
			// 
			// txtmin
			// 
			this->txtmin->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtmin->Location = System::Drawing::Point(281, 63);
			this->txtmin->Name = L"txtmin";
			this->txtmin->Size = System::Drawing::Size(100, 22);
			this->txtmin->TabIndex = 4;
			this->txtmin->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->txtmin->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtmin_KeyPress);
			// 
			// txtmax
			// 
			this->txtmax->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtmax->Location = System::Drawing::Point(281, 97);
			this->txtmax->Name = L"txtmax";
			this->txtmax->Size = System::Drawing::Size(100, 22);
			this->txtmax->TabIndex = 5;
			this->txtmax->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->txtmax->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtmax_KeyPress);
			// 
			// txtkol
			// 
			this->txtkol->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtkol->Location = System::Drawing::Point(281, 27);
			this->txtkol->Name = L"txtkol";
			this->txtkol->Size = System::Drawing::Size(100, 22);
			this->txtkol->TabIndex = 3;
			this->txtkol->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->txtkol->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtkol_KeyPress);
			// 
			// label2
			// 
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->label2->Location = System::Drawing::Point(6, 97);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(257, 23);
			this->label2->TabIndex = 2;
			this->label2->Text = L"������������ �������� ���������:";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->label1->Location = System::Drawing::Point(6, 63);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(244, 23);
			this->label1->TabIndex = 1;
			this->label1->Text = L"����������� �������� ���������: ";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// lbl1
			// 
			this->lbl1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lbl1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->lbl1->Location = System::Drawing::Point(6, 26);
			this->lbl1->Name = L"lbl1";
			this->lbl1->Size = System::Drawing::Size(231, 23);
			this->lbl1->TabIndex = 0;
			this->lbl1->Text = L"���������� ��������� �������: ";
			this->lbl1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txtMass
			// 
			this->txtMass->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtMass->Location = System::Drawing::Point(12, 176);
			this->txtMass->Name = L"txtMass";
			this->txtMass->Size = System::Drawing::Size(381, 20);
			this->txtMass->TabIndex = 6;
			this->txtMass->TextChanged += gcnew System::EventHandler(this, &Form1::txtMass_TextChanged);
			this->txtMass->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtMass_KeyPress);
			this->txtMass->Leave += gcnew System::EventHandler(this, &Form1::txtMass_Leave);
			// 
			// label3
			// 
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->label3->Location = System::Drawing::Point(12, 150);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(231, 23);
			this->label3->TabIndex = 6;
			this->label3->Text = L"�������� ������:";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->rb8);
			this->groupBox2->Controls->Add(this->rb7);
			this->groupBox2->Controls->Add(this->rb6);
			this->groupBox2->Controls->Add(this->rb5);
			this->groupBox2->Controls->Add(this->rb4);
			this->groupBox2->Controls->Add(this->rb3);
			this->groupBox2->Controls->Add(this->rb2);
			this->groupBox2->Controls->Add(this->rb1);
			this->groupBox2->ForeColor = System::Drawing::SystemColors::HotTrack;
			this->groupBox2->Location = System::Drawing::Point(12, 204);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(387, 116);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"�������� � ��������";
			// 
			// rb8
			// 
			this->rb8->AutoSize = true;
			this->rb8->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb8->Location = System::Drawing::Point(207, 89);
			this->rb8->Name = L"rb8";
			this->rb8->Size = System::Drawing::Size(154, 17);
			this->rb8->TabIndex = 7;
			this->rb8->TabStop = true;
			this->rb8->Text = L"���������� �� ��������\r\n";
			this->rb8->UseVisualStyleBackColor = true;
			this->rb8->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb8_CheckedChanged);
			// 
			// rb7
			// 
			this->rb7->AutoSize = true;
			this->rb7->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb7->Location = System::Drawing::Point(207, 66);
			this->rb7->Name = L"rb7";
			this->rb7->Size = System::Drawing::Size(170, 17);
			this->rb7->TabIndex = 6;
			this->rb7->TabStop = true;
			this->rb7->Text = L"���������� �� �����������\r\n";
			this->rb7->UseVisualStyleBackColor = true;
			this->rb7->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb7_CheckedChanged);
			// 
			// rb6
			// 
			this->rb6->AutoSize = true;
			this->rb6->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb6->Location = System::Drawing::Point(207, 43);
			this->rb6->Name = L"rb6";
			this->rb6->Size = System::Drawing::Size(129, 17);
			this->rb6->TabIndex = 5;
			this->rb6->TabStop = true;
			this->rb6->Text = L"�������� ��������\r\n";
			this->rb6->UseVisualStyleBackColor = true;
			this->rb6->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb6_CheckedChanged);
			// 
			// rb5
			// 
			this->rb5->AutoSize = true;
			this->rb5->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb5->Location = System::Drawing::Point(207, 20);
			this->rb5->Name = L"rb5";
			this->rb5->Size = System::Drawing::Size(118, 17);
			this->rb5->TabIndex = 4;
			this->rb5->TabStop = true;
			this->rb5->Text = L"������ ��������\r\n";
			this->rb5->UseVisualStyleBackColor = true;
			this->rb5->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb5_CheckedChanged);
			// 
			// rb4
			// 
			this->rb4->AutoSize = true;
			this->rb4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb4->Location = System::Drawing::Point(9, 89);
			this->rb4->Name = L"rb4";
			this->rb4->Size = System::Drawing::Size(150, 17);
			this->rb4->TabIndex = 3;
			this->rb4->TabStop = true;
			this->rb4->Text = L"������������ �������\r\n";
			this->rb4->UseVisualStyleBackColor = true;
			this->rb4->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb4_CheckedChanged);
			// 
			// rb3
			// 
			this->rb3->AutoSize = true;
			this->rb3->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb3->Location = System::Drawing::Point(9, 66);
			this->rb3->Name = L"rb3";
			this->rb3->Size = System::Drawing::Size(144, 17);
			this->rb3->TabIndex = 2;
			this->rb3->TabStop = true;
			this->rb3->Text = L"����������� �������\r\n";
			this->rb3->UseVisualStyleBackColor = true;
			this->rb3->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb3_CheckedChanged);
			// 
			// rb2
			// 
			this->rb2->AutoSize = true;
			this->rb2->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb2->Location = System::Drawing::Point(9, 43);
			this->rb2->Name = L"rb2";
			this->rb2->Size = System::Drawing::Size(118, 17);
			this->rb2->TabIndex = 1;
			this->rb2->TabStop = true;
			this->rb2->Text = L"������� ��������\r\n";
			this->rb2->UseVisualStyleBackColor = true;
			this->rb2->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb2_CheckedChanged);
			// 
			// rb1
			// 
			this->rb1->AutoSize = true;
			this->rb1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->rb1->Location = System::Drawing::Point(9, 20);
			this->rb1->Name = L"rb1";
			this->rb1->Size = System::Drawing::Size(117, 17);
			this->rb1->TabIndex = 0;
			this->rb1->TabStop = true;
			this->rb1->Text = L"����� ���������\r\n";
			this->rb1->UseVisualStyleBackColor = true;
			this->rb1->CheckedChanged += gcnew System::EventHandler(this, &Form1::rb1_CheckedChanged);
			// 
			// label4
			// 
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label4->ForeColor = System::Drawing::SystemColors::ControlText;
			this->label4->Location = System::Drawing::Point(12, 323);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(231, 23);
			this->label4->TabIndex = 8;
			this->label4->Text = L"��������� ��������:";
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txtRes
			// 
			this->txtRes->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtRes->Location = System::Drawing::Point(12, 349);
			this->txtRes->Name = L"txtRes";
			this->txtRes->ReadOnly = true;
			this->txtRes->Size = System::Drawing::Size(381, 20);
			this->txtRes->TabIndex = 7;
			// 
			// btnClose
			// 
			this->btnClose->Location = System::Drawing::Point(405, 348);
			this->btnClose->Name = L"btnClose";
			this->btnClose->Size = System::Drawing::Size(116, 23);
			this->btnClose->TabIndex = 9;
			this->btnClose->Text = L"�������";
			this->btnClose->UseVisualStyleBackColor = true;
			this->btnClose->Click += gcnew System::EventHandler(this, &Form1::btnClose_Click);
			// 
			// btnEnter
			// 
			this->btnEnter->Location = System::Drawing::Point(405, 204);
			this->btnEnter->Name = L"btnEnter";
			this->btnEnter->Size = System::Drawing::Size(116, 23);
			this->btnEnter->TabIndex = 10;
			this->btnEnter->Text = L"���������";
			this->btnEnter->UseVisualStyleBackColor = true;
			this->btnEnter->Click += gcnew System::EventHandler(this, &Form1::btnEnter_Click);
			// 
			// btngen
			// 
			this->btngen->Location = System::Drawing::Point(405, 12);
			this->btngen->Name = L"btngen";
			this->btngen->Size = System::Drawing::Size(116, 23);
			this->btngen->TabIndex = 11;
			this->btngen->Text = L"��������� �������";
			this->btngen->UseVisualStyleBackColor = true;
			this->btngen->Click += gcnew System::EventHandler(this, &Form1::btngen_Click);
			// 
			// btnReset
			// 
			this->btnReset->Location = System::Drawing::Point(405, 175);
			this->btnReset->Name = L"btnReset";
			this->btnReset->Size = System::Drawing::Size(116, 23);
			this->btnReset->TabIndex = 12;
			this->btnReset->Text = L"�����";
			this->btnReset->UseVisualStyleBackColor = true;
			this->btnReset->Click += gcnew System::EventHandler(this, &Form1::btnReset_Click);
			// 
			// label5
			// 
			this->label5->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(405, 53);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(116, 19);
			this->label5->TabIndex = 15;
			this->label5->Text = L"��� ����� �����:";
			this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txtFileIn
			// 
			this->txtFileIn->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->txtFileIn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtFileIn->Location = System::Drawing::Point(405, 79);
			this->txtFileIn->Name = L"txtFileIn";
			this->txtFileIn->Size = System::Drawing::Size(116, 20);
			this->txtFileIn->TabIndex = 14;
			// 
			// btnIn
			// 
			this->btnIn->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->btnIn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->btnIn->Location = System::Drawing::Point(405, 108);
			this->btnIn->Name = L"btnIn";
			this->btnIn->Size = System::Drawing::Size(116, 27);
			this->btnIn->TabIndex = 13;
			this->btnIn->Text = L"���� �� �����";
			this->btnIn->UseVisualStyleBackColor = true;
			this->btnIn->Click += gcnew System::EventHandler(this, &Form1::btnIn_Click);
			// 
			// label6
			// 
			this->label6->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label6->Location = System::Drawing::Point(405, 245);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(116, 19);
			this->label6->TabIndex = 18;
			this->label6->Text = L"��� ����� ������:";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txtFileOut
			// 
			this->txtFileOut->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->txtFileOut->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtFileOut->Location = System::Drawing::Point(405, 267);
			this->txtFileOut->Name = L"txtFileOut";
			this->txtFileOut->Size = System::Drawing::Size(116, 20);
			this->txtFileOut->TabIndex = 17;
			// 
			// btnOut
			// 
			this->btnOut->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->btnOut->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->btnOut->Location = System::Drawing::Point(405, 299);
			this->btnOut->Name = L"btnOut";
			this->btnOut->Size = System::Drawing::Size(116, 27);
			this->btnOut->TabIndex = 16;
			this->btnOut->Text = L"��������� � ����";
			this->btnOut->UseVisualStyleBackColor = true;
			this->btnOut->Click += gcnew System::EventHandler(this, &Form1::btnOut_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(533, 395);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->txtFileOut);
			this->Controls->Add(this->btnOut);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->txtFileIn);
			this->Controls->Add(this->btnIn);
			this->Controls->Add(this->btnReset);
			this->Controls->Add(this->btngen);
			this->Controls->Add(this->btnEnter);
			this->Controls->Add(this->btnClose);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->txtRes);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->txtMass);
			this->Controls->Add(this->groupBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"��������� �������";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private:void quickSort(int* numbers, int left, int right){
		  int pivot; // ����������� �������
		  int l_hold = left; //����� �������
		  int r_hold = right; // ������ �������
		  pivot = numbers[left];
		  while (left < right) // ���� ������� �� ���������
		  {
			while ((numbers[right] >= pivot) && (left < right))
			  right--; // �������� ������ ������� ���� ������� [right] ������ [pivot]
			if (left != right) // ���� ������� �� ����������
			{
			  numbers[left] = numbers[right]; // ���������� ������� [right] �� ����� ������������
			  left++; // �������� ����� ������� ������ 
			}
			while ((numbers[left] <= pivot) && (left < right))
			  left++; // �������� ����� ������� ���� ������� [left] ������ [pivot]
			if (left != right) // ���� ������� �� ����������
			{
			  numbers[right] = numbers[left]; // ���������� ������� [left] �� ����� [right]
			  right--; // �������� ������ ������� ������ 
			}
		  }
		  numbers[left] = pivot; // ������ ����������� ������� �� �����
		  pivot = left;
		  left = l_hold;
		  right = r_hold;
		  if (left < pivot) // ���������� �������� ���������� ��� ����� � ������ ����� �������
			quickSort(numbers, left, pivot - 1);
		  if (right > pivot)
			quickSort(numbers, pivot + 1, right);
		}
private: System::Void txtMass_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 txtRes->Text="";
			 proverka=0;
			 for (int i=1;i<=8;i++)
				 deystvie[i]=0;
		 }	
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 rb1->Checked=true;
			 }


private: System::Void btnClose_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
private: System::Void btnEnter_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(txtMass->Text!="")
			 {
			 txtRes->Text=L"";
			 int a[100]={0},kol=0,i=0;//kol-���������� ���������� �������
									  //i-������� � ������
			 int temp=0,minus=1;//temp-������� �����������, ��� ����� �������
								//minus-������������ ������� �������?
			 //���� ������� �� ����������;
			 String^ g=txtMass->Text+" ";
			 int l=g->Length;
			 while(i<l)
			 {
				//���� ��� ������� ������� ������
				if(i>0)
				 if(g[i]==' ' && g[i-1]==' ')
					goto perehod;
				//���� ���������� �����, �� �������� ����� �������������
				if(g[i]=='-')
					minus=-1;
				//���� ������� �����,�� ���� ������������ ������,
				//���� ���� ��� �� ����� ������� ������ ����������� ���
				//������� ������
				else if(g[i]>='0' && g[i]<='9')
				{
					int perem=(int)g[i]-48;
					temp=temp*10+perem;
				}
				//���� ������ ������ �� ���� ����������� �������� �������
				else if(g[i]==' ')
				{
					int odinakovie=0;
					for(int i=0; i<kol;i++)
						if(a[kol]==a[i])
							odinakovie=1;
					if(odinakovie==0)
					{
						a[kol]=minus*temp;
						kol++;
					}
					minus=1;
					temp=0;
				}
				//������� ������� � ������
				perehod:
				i++;
			 }

			 int res1=0,t,t1;
			 double res2;
			 String^ r;

			 txtMass->Text="";
			 for(int i=0;i<kol;i++)
				 txtMass->Text+=Convert::ToInt32(a[i])+" ";
			 //� t ����� �������� ����� ���������� ��������
			 //����������� ����� radiobutton �������
			 if(rb1->Checked==true){
				txtRes->Text=L"����� ����������: ";
				//������ ������� ������� ������������ � 0
				for(int i=0;i<kol;i++)
					res1+=a[i];
				txtRes->Text+=Convert::ToString(res1);
			 }
			 else if(rb2->Checked==true){
				txtRes->Text=L"������� ��������: ";
				//������� ������� ����� ���� ��������� �������
				for(int i=0;i<kol;i++)
					res1+=a[i];
				//� ����� �� ���������� ���������
				res2=(double)res1/kol;
				//�������������� ���� ���� ����� �������
				r=String::Format("{0:F1}",res2);
				txtRes->Text+=r;
			 }
			 else if(rb3->Checked==true){
				txtRes->Text=L"����������� ������: ";
				//����������� ��� ����������� ������� ������
				res1=a[0];
				//������ ������ ������� ��� �� ����� ����������
				for(int i=1;i<kol;i++)
					//���� ���� �����-�� ������� ������, �� ��������������� res1
					if(res1>a[i])
						res1=a[i];
				txtRes->Text+=Convert::ToString(res1);
			 }
			 else if(rb4->Checked==true){
				txtRes->Text=L"������������ �������: ";
				//����� ��� � ������� ������������ ��������
				res1=a[0];
				for(int i=1;i<kol;i++)
					if(res1<a[i])
						res1=a[i];
				txtRes->Text+=Convert::ToString(res1);
			 }
			 if(rb5->Checked==true){
				for(int i=0;i<kol;i++)
				//���� ������� ������ �������
				if(a[i]%2==0)
					txtRes->Text+=Convert::ToString(a[i])+" ";
			 }
			 if(rb6->Checked==true){
				txtRes->Text=L"�������� ��������: ";
				//����� ��� � ������� ����������
				for(int i=0;i<kol;i++)
					if(abs(a[i])%2==1)
						txtRes->Text+=Convert::ToString(a[i])+" ";
			 }
			 if(rb7->Checked==true)
			 {
				 txtRes->Text=L"���������� �� �����������: ";
				//C���������
				quickSort(a, 0, kol-1);
				//����� ���������������� �������
				for(int i=0; i<kol; i++)
				txtRes->Text+=Convert::ToString(a[i])+L" ";
			 }
			 if(rb8->Checked==true){
				txtRes->Text=L"���������� �� ��������: ";
				//C���������
				quickSort(a, 0, kol-1);
				//����� ���������������� ������� � �������� �������
				for(int i=kol-1; i>=0; i--)
				txtRes->Text+=Convert::ToString(a[i])+L" ";
			 }			
			 	
			 }
			 else
				 MessageBox::Show(this,"�� ���������� ���� ��������� �������. ��������� ����.","������",MessageBoxButtons::OK,MessageBoxIcon::Error);
			 				
		 }
private: System::Void btngen_Click(System::Object^  sender, System::EventArgs^  e) {
			 //�������� ��������� �� ������� ����������� ���
			 //�������� ������� ���������
			 //������� ����� ������������ � ����������� ���������� ������ ���� >= 0

			 if (txtkol->Text=="" || txtkol->Text=="0")
			 {
				//����� ���������, ��� �� ��������� ������ ���������� ��������� �������
				MessageBox::Show(this,"�� ���������� �������� ���������� ��������� �������. ��������� ����.","������",MessageBoxButtons::OK,MessageBoxIcon::Error);
				//������� ���� ��� ���������� �����
				this->txtkol->Text=L"";
			 }
			 else if(txtmax->Text=="" || txtmin->Text=="" || (Convert::ToInt32(txtmax->Text)-Convert::ToInt32(txtmin->Text))<0)
			 {
				//����� ���������, ��� �� ��������� ����� ��������
				MessageBox::Show(this,"�� ���������� �������� ��������� ���������� �������� �������. ��������� ����.","������",MessageBoxButtons::OK,MessageBoxIcon::Error);
				//������� ����� ��� ���������� �����
				if(txtmax->Text=="")
					this->txtmax->Text=L"";
				if(txtmin->Text=="")
					this->txtmin->Text=L"";
				if(Convert::ToInt32(txtmax->Text)-Convert::ToInt32(txtmin->Text)<0)
				{
					this->txtmax->Text=L"";
					this->txtmin->Text=L"";
				}
					
			 }
			 else
			 {
				 //��������������� ������� ���� ��� ����������
				 txtMass->Text=L"";
				 int kol;//���������� ��������� �������
				 int max,min;//�������� ���������� ��������
				 int a[100];//����������� ������
				 //���� ������
				 kol=Convert::ToInt32(txtkol->Text);
				 max=Convert::ToInt32(txtmax->Text);
				 min=Convert::ToInt32(txtmin->Text);
				 //�������� ���������� ���� random
				 Random^ generator=gcnew Random;
				 for (int i=1;i<=kol;i++)
				 {
					 a[i]=(generator->Next(max-min+1))+min;
					 //����� ����������� � ������� ������� ��������� +1
					 //������ �������� ��������� ��� ������� � ������
					 if (i==1)
						 txtMass->Text+=Convert::ToString(a[i]);
					 else
						 txtMass->Text+=" "+ Convert::ToString(a[i]);
				 }
			 }
		 }
private: System::Void btnReset_Click(System::Object^  sender, System::EventArgs^  e) {
			 txtMass->Text=L"";
			 txtkol->Text=L"";
			 txtmax->Text=L"";
			 txtmin->Text=L"";
			 txtRes->Text="";
			 proverka=0;
		 }
private: System::Void txtMass_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 // ���� ������� ������� �� ��������.
			  if (!Char::IsDigit(e->KeyChar))
				 if (e->KeyChar != '-')
					if (e->KeyChar != ' ')
					   if (e->KeyChar != (char)Keys::Back)
						  e->Handled = true;          // ������ �����			  

			  // ������� ����� ����� ������ � ������ (���� ������ ������ �������)
			  if (e->KeyChar == '-' && txtmax->Text->IndexOf('-') == -1)
				 txtmax->SelectionStart = 0;         // ��������� ������ � ������
		 }
private: System::Void btnIn_Click(System::Object^  sender, System::EventArgs^  e) {
			 txtMass->Text=""; 
			 // �������� ������� ����������� ���� OpenFileDialog
			  OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog;

			  // ��������� ������� ����������� ����
			  openFileDialog1->Title = "�������� �����";
			  openFileDialog1->InitialDirectory = "c:\\";
			  openFileDialog1->Filter ="txt files (*.txt)|*.txt|All files (*.*)|*.*";
			  openFileDialog1->FilterIndex = 1;
			  openFileDialog1->ShowReadOnly = true;
			  openFileDialog1->RestoreDirectory = true;

			  // �������� ����������� ���� � ������ ������� �����
			  // ���������� � ���� ����� � ��������� ����
			  if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK) 
			  {
				 txtFileIn->Text = openFileDialog1->FileName;
				 // �������� �������� �����, ������ �� ���� � ��������
				 StreamReader^ sr = gcnew StreamReader(openFileDialog1->FileName);
				 txtMass->Text=sr->ReadLine()+" ";
			  }
			  else
			  {
				 txtFileIn->Text = "";
			  }
		 }
private: System::Void btnOut_Click(System::Object^  sender, System::EventArgs^  e) {
			  // �������� ������� ����������� ���� SaveFileDialog
			  SaveFileDialog^ saveFileDialog1 = gcnew SaveFileDialog();

			  // ��������� ������� ����������� ����
			  saveFileDialog1->Title = "���������� �����";
			  saveFileDialog1->Filter ="Text files (*.txt)|*.txt|All files (*.*)|*.*";
			  saveFileDialog1->FilterIndex = 1;
			  saveFileDialog1->RestoreDirectory = true;
			  saveFileDialog1->OverwritePrompt = false;

			  // �������� ����������� ���� � ������ ������� �����
			  // ��������� � ���� ����� � ��������� ����
			  if (saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK) 
			  {
				 txtFileOut->Text = saveFileDialog1->FileName;
				 String^ g=saveFileDialog1->FileName;
				 // �������� �������� �����, ������ � ���� � ��������
				 if(txtRes->Text!="")
				 {
					 String^ p=File::ReadAllText(g);
					 String^ res=txtRes->Text;
					 StreamWriter^ sw = gcnew StreamWriter(g,true);//������� ���� ��� ��������
					 if(rb1->Checked==true)
						 if(deystvie[1]==0)
							deystvie[1]=1;
						 else
							 goto err;
					 if(rb2->Checked==true)
						 if(deystvie[2]==0)
							deystvie[2]=1;
						 else
							 goto err;
					 if(rb3->Checked==true)
						 if(deystvie[3]==0)
							deystvie[3]=1;
						 else
							 goto err;
					 if(rb4->Checked==true)
						 if(deystvie[4]==0)
							deystvie[4]=1;
						 else
							 goto err;
					 if(rb5->Checked==true)
						 if(deystvie[5]==0)
							deystvie[5]=1;
						 else
							 goto err;
					 if(rb6->Checked==true)
						 if(deystvie[6]==0)
							deystvie[6]=1;
						 else
							 goto err;
					 if(rb7->Checked==true)
						 if(deystvie[7]==0)
							deystvie[7]=1;
						 else
							 goto err;
					 if(rb8->Checked==true)
						 if(deystvie[8]==0)
							deystvie[8]=1;
						 else
							 goto err;
					 if(proverka==0)
					 {	
						if(p!="")
						{
							sw->WriteLine();
							sw->WriteLine("---------------------------------------------------------------------------------");
							sw->WriteLine();
						}
						sw->WriteLine("�������� ������ : "+txtMass->Text);
						sw->WriteLine();
						sw->WriteLine(res);
						/*sw->WriteLine();*/
						proverka=1;
					 }
					 else
					 {
						 sw->WriteLine(res);
					 }
					 g="���������� ������ �������� � ����� "+g;
					 MessageBox::Show(this,g,"����������",MessageBoxButtons::OK,MessageBoxIcon::Information);
					 err:
					 sw->Close();	
				 }
				 else
				 {
					 MessageBox::Show(this,"��� ���������� ���������� �������� ����� �������� � ������� \"���������\"","����������",MessageBoxButtons::OK,MessageBoxIcon::Information);
				 }
			  }			  
			  else
			  {
				 txtFileOut->Text = "";
			  }
			 
      
		 }

private: System::Void txtkol_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 // ���� ������� ������� �� ��������.
			  if (!Char::IsDigit(e->KeyChar))
				  // ���� ������� ������� �� �������� �������� BackSpace.
				  if (e->KeyChar != (char)Keys::Back)
					  e->Handled = true; // ������ �����

			  // ������ ����� ��������� ����� � ������ �����.
			  if (e->KeyChar == (char)Keys::D0 )  // ���� ������ ������� 0
				 if (txtkol->Text->Length >= 1)        // ���� ������ �� ������
					if (txtkol->Text[0] == '0' && txtkol->SelectionStart < 2)
					   e->Handled = true;             // ������ �����

			// ���� ����� �y�� ���� ����� ������� ����
			if (Char::IsDigit(e->KeyChar) && e->KeyChar!= (char)Keys::D0 )
			if (txtkol->Text=="0")
			{
					txtkol->Text=L"";
					txtkol->SelectionStart=0;
			}
		 }
private: System::Void txtmin_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 // ���� ������� ������� �� ��������.
			  if (!Char::IsDigit(e->KeyChar))
				 // ������ �� ���� ����� ������ ����� ������.
				 if (e->KeyChar != '-' || txtmin->Text->IndexOf('-') != -1)
					   // ���� ������� ������� �� �������� �������� BackSpace.
					   if (e->KeyChar != (char)Keys::Back)
						  e->Handled = true;          // ������ �����

			  // ������ ����� ��������� ����� � ������ �����.
			  if (e->KeyChar == (char)Keys::D0 ||
				  e->KeyChar == (char)Keys::NumPad0)  // ���� ������ ������� 0
				 if (txtmin->Text->Length >= 1)        // ���� ������ �� ������
					if (txtmin->Text[0] == '0' && txtmin->SelectionStart < 2)
					   e->Handled = true;             // ������ �����

			  

			  // ������� ����� ����� ������ � ������ (���� ������ ������ �������)
			  if (e->KeyChar == '-' && txtmin->Text->IndexOf('-') == -1)
				 txtmin->SelectionStart = 0;         // ��������� ������ � ������

			// ���� ����� ���� ���� ����� ������� ����
			if (Char::IsDigit(e->KeyChar) && (e->KeyChar!= (char)Keys::D0 || e->KeyChar != (char)Keys::NumPad0))
			if (txtmin->Text=="0")
			{
					txtmin->Text=L"";
					txtmin->SelectionStart=0;
			}
		 }
private: System::Void txtmax_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 // ���� ������� ������� �� ��������.
			  if (!Char::IsDigit(e->KeyChar))
				 // ������ �� ���� ����� ������ ����� ������.
				 if (e->KeyChar != '-' || txtmax->Text->IndexOf('-') != -1)
					   // ���� ������� ������� �� �������� �������� BackSpace.
					   if (e->KeyChar != (char)Keys::Back)
						  e->Handled = true;          // ������ �����

			  // ������ ����� ��������� ����� � ������ �����.
			  if (e->KeyChar == (char)Keys::D0 ||
				  e->KeyChar == (char)Keys::NumPad0)  // ���� ������ ������� 0
				 if (txtmax->Text->Length >= 1)        // ���� ������ �� ������
					if (txtmax->Text[0] == '0' && txtmax->SelectionStart < 2)
					   e->Handled = true;             // ������ �����

			  

			  // ������� ����� ����� ������ � ������ (���� ������ ������ �������)
			  if (e->KeyChar == '-' && txtmax->Text->IndexOf('-') == -1)
				 txtmax->SelectionStart = 0;         // ��������� ������ � ������

			// ���� ����� ���� ���� ����� ������� ����
			if (Char::IsDigit(e->KeyChar) && (e->KeyChar!= (char)Keys::D0 || e->KeyChar != (char)Keys::NumPad0))
			if (txtmax->Text=="0")
			{
					txtmax->Text=L"";
					txtmax->SelectionStart=0;
			}
		 }
private: System::Void rb1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb6_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb7_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void rb8_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			txtRes->Text="";
		 }
private: System::Void txtMass_Leave(System::Object^  sender, System::EventArgs^  e) {
			 String^ g=txtMass->Text;
			 int l=txtMass->Text->Length,i=0;
			 while(g[i]==' ')
			 {
				l--;
				g=g->String::Remove(i,1);
			 }
			 while(i<l)
			 {
				 if(g[i]==' ' && g[i+1]==' ')
				 {
					 g=g->String::Remove(i,1);
					 l--;
				 }
				 else
					i++;
			 }
			 txtMass->Text=g;

		 }
};
}

